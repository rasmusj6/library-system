//-----------------------------------------------------------------------------
// bintree.h
// **BinTree class definition
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------
// BinTree Class: BinTree is a sorted collection of InventoryItem objects.
//	-- operations included for insert, remove, retrieve, and display
//
// Assumptions:
//	-- InventoryItem will handle its own comparison operators
//	-- Duplicate items are not inserted into tree
//-----------------------------------------------------------------------------

#ifndef BINTREE_H
#define BINTREE_H

#include "InventoryItem.h"

using namespace std;

//-----------------------------Node Class--------------------------------------
class Node { 

public:

//------------------------------constructor------------------------------------
// sets data to *data and NULL children
	Node(InventoryItem* data) : data(data), left(NULL), right(NULL) {}

	InventoryItem* data; // pointer to data object 
	Node* left;			 // left subtree pointer 
	Node* right;		 // right subtree pointer 
}; 

//----------------------------BinTree Class------------------------------------
class BinTree { 

public: 

//----------------------------constructor--------------------------------------
// sets NULL root
	BinTree();

//-----------------------------destructor--------------------------------------
//  calls makeEmpty
	~BinTree();

//-------------------------------isEmpty---------------------------------------
//  True if tree is empty
	bool isEmpty() const;

//-----------------------------makeEmpty---------------------------------------
//  Removes all nodes and data from the tree
	void makeEmpty();

//------------------------------display----------------------------------------
//  Prints the contest of the tree in sequential order
	void display() const;

//-------------------------------Insert----------------------------------------
//  New node is inserted into the tree pointing to the data provided, 
//	successful insert returns true
	bool insert(InventoryItem*); 

//-------------------------------remove----------------------------------------
//  Removes the InventoryItem ToRemove from the tree and Removed
//  points to the removed data; User must delete the removed data if desired.
	bool remove(const InventoryItem &, InventoryItem*&);

//-----------------------------------------------------------------------------
//  Finds target in the search tree and assigns it to found. True is returned 
//  if the target is found
	bool retrieve(const InventoryItem & target, InventoryItem*& found) const;

private: 

	Node* root; // root of the tree 
 
//----------------------Utility/Helper Functions-------------------------------

	bool removeHelper(Node*& Current, const InventoryItem& ToRemove, 
					  InventoryItem*& Removed);

	void makeEmptyHelper(Node*& n);

	void deleteRoot(Node *&root);

	InventoryItem* findAndDeleteMostLeft(Node *&root);

	friend ostream& printNodeData(ostream & out, const Node * Curr);

	void displayHelper(const Node*) const;

}; 

#endif
