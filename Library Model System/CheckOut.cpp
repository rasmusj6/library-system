//-----------------------------------------------------------------------------
// CheckOut.cpp
// **CheckOut member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------


#include "CheckOut.h"

//------------------------------constructor------------------------------------
CheckOut::CheckOut() { 
	user = NULL;
	item = NULL;
	performed = false;
}

//---------------------------------set-----------------------------------------
//  Set data members of Action
//  Preconditions:  A valid Library array and patron array has been created, 
//					the input line formatted as:
//                  <patronNumber>' '<ItemTypeCode>' '<formatCode>' '
//                  <primarySearchCriteria>", "<secondarySearchCriteria>','
//					ex. "8888 F H Lawrence D. H., Lady Chatterley's Lover,"
//
//  Postconditions: Library l is assigned to the actions lib, the patron 
//					defined by patronNumber is assigned to user, 
//					and the library item is assigned to item
bool CheckOut::set(string &s, Library* l, Patron** p){
	
	if( !performed && l != NULL && p != NULL){

		int end = s.find(' '); // End of patron number
		string patronNumberString = s.substr(0,end); // Extract patron number
		//Assign patron user
		int patronNumber = stoi2(patronNumberString);

		if( patronNumber >= 0 && patronNumber < kPatronSize ){
			user = p[patronNumber]; // Find patron by number

			if (user != NULL) {         // if user exists
				string s2 = s.substr(5);// search criteria
				item = l->find(s2);     // Assign item
				// If the item is found, return true
				if( item != NULL ) return true;
			}
		}
	}
	return false;
}

//--------------------------------perform--------------------------------------
//  Perform the action checking out the item to the user
//  Preconditions:  CheckOut has been properly set
//  Postconditions: Item is checked out to user, and this action is included 
//                  in the users history
bool CheckOut::perform(){

	if( !performed && user != NULL && item != NULL ){

		performed = true;
	
	//mark checked out && add to patrons collection && add to patrons history
		return (item->checkOut() &&			// Item attempts to checkout
				user->checkOut(item) &&		// User attempts to add to items
				user->addAction(this));		// Action is added to history
	}
	return false;
}

//--------------------------------display--------------------------------------
//  Display the contents of the action
//  Preconditions:  The action has been set with valid data and performed
//  Postconditions: The action identifies itself as a CheckOut and displays 
//					the items contents
void CheckOut::display() const{
	if(performed){
		cout << "CheckOut ";
		item->pDisplay();
	}
}

//------------------------------- create --------------------------------------
//  Produce a new action of the same type as this action
//  Preconditions:  This has been properly constructed.
//  Postconditions: A pointer to a new action of this type is returned.
//					NOTE: User is responsible for deleting this new action.
Action* CheckOut::create() const{
	
	return new CheckOut(); // return pointer to new copy of same Action
}
