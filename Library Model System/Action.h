//-----------------------------------------------------------------------------
// Action.h
// **Base class Action definition
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef _ACTION_H_
#define _ACTION_H_

#include <string>
#include "Library.h"
#include "Patron.h"

class Patron;

//------------------------------Action Class-----------------------------------
// Action Class: is a base class that all library actions are derived from
//	-- set function to Set data members of Action, 
//     overridden in derived classes
//	-- perform function to execute operations of the action, overridden in 
//     derived classes
//  -- display function to display output of the action and its related 
//     parties, overridden in derived classes
//
// Assumptions:
//	-- Action objects are created by a system with at least one Library and 
//     at least one Patron
//-----------------------------------------------------------------------------
class Action{
	
public:

//------------------------------constructor------------------------------------
	Action(){ performed = false; }

//------------------------------destructor-------------------------------------
	virtual ~Action(){}

//---------------------------------set-----------------------------------------
// pure virtual(must be implemented in children class)
	virtual bool set(string&, Library*, Patron**) = 0; //Needs all patrons/lib

//--------------------------------perform--------------------------------------
// pure virtual(must be implemented in children class)
	virtual bool perform() = 0;

//--------------------------------display--------------------------------------
// pure virtual(must be implemented in children class)
	virtual void display() const = 0;

//--------------------------------create---------------------------------------
// pure virtual(must be implemented in children class)
	virtual Action* create() const = 0;

protected:

	bool performed; //Marker for action to indicate if perform has been called
};

#endif
