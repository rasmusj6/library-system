//-----------------------------------------------------------------------------
// InventoryItem.cpp
// **InventoryItem member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------


#include "InventoryItem.h"

//-------------------------- constructor --------------------------------------
// Constructs InventoryItem
// Precondition:  None
// Postcondition: checkedOut is set 0
InventoryItem::InventoryItem() {
	checkedOut = 0;
}

// empty destructor
InventoryItem::~InventoryItem() {}

//-------------------------- checkOut() ---------------------------------------
// Increments the counter for number for "this" InventoryItem that have been
// checked out "checkedOut"
// Precondition:  InventoryItem is properly constructed
// Postcondition: checkedOut increases by 1. Return true if successful 
bool InventoryItem::checkOut() {

	if(checkedOut < totalCnt) { // if not all InventoryItem are checked out
		checkedOut++;
		return true;
	}
	return false;
}

//-------------------------- checkIn() ----------------------------------------
// Decrements the counter for number for "this" InventoryItem that have been
// checked out "checkOut"
// Precondition:  InventoryItem is properly constructed
// Postcondition: checkedOut decreases by 1. Return true if successful 
bool InventoryItem::checkIn() {
	
	if(checkedOut > 0) { // if no InventoryItem are checked out
		checkedOut--;
		return true;
	}
	return false;
}
