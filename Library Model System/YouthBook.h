//-----------------------------------------------------------------------------
// YouthBook.h
// **Derived class YouthBook
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef YOUTHBOOK_H
#define YOUTHBOOK_H_

#include "Book.h"

using namespace std;

//-----------------------------------------------------------------------------
// YouthBook Class: derived class of book
//	-- Defines comparison operators for YouthBooks(Title then Author)
//
// Assumptions:
//	-- receives display functionality and set functionality from Book
//-----------------------------------------------------------------------------
class YouthBook: public Book{

public:
//----------------------------constructor--------------------------------------
	YouthBook();

//-----------------------------destructor--------------------------------------
	virtual ~YouthBook();

//-------------------------------set-------------------------------------------
	//bool set(string);

	bool searchSet(string&);

	InventoryItem* create() const;

//-----------------------------Operators---------------------------------------
	bool operator<(const InventoryItem &b) const; 
	bool operator>(const InventoryItem &b) const;
	bool operator==(const InventoryItem &b) const;
	bool operator!=(const InventoryItem &b) const;
	bool operator<=(const InventoryItem &b) const; 
	bool operator>=(const InventoryItem &b) const;

};

#endif
