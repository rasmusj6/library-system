//-----------------------------------------------------------------------------
// Library.h
// **Library class definition
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef _LIBRARY_H_
#define _LIBRARY_H_

#include "InventoryItem.h"
#include "ItemFactory.h"
#include "bintree.h"
#include "constants.h"
#include <iostream>

using namespace std;



//-------------------------------Library Class---------------------------------
// Library Class: Collection of LibraryItems sorted by format and then by type
//	-- find: returns pointer to the InventoryItem specified by a string
//	-- registerItem: allows an InventoryItem to be added to the collection by 
//     type and then by format
//	-- checkIn: increments on hand amount of an InventoryItem
//	-- checkOut: decrements on hand amount of an InventoryItem
//  -- displayContents: displays the contents of the entire Library
//
// Assumptions:
//	-- InventoryItem is responsible for its own comparison operations
//-----------------------------------------------------------------------------
class Library{

public:

//----------------------------constructor--------------------------------------
	Library();

//-----------------------------destructor--------------------------------------
	~Library();

//-------------------------------find------------------------------------------
	InventoryItem* find(string&) const;

//----------------------------registerItem-------------------------------------
// Adds an InventoryItem to the collection coded by [format][type]
	bool registerItem(InventoryItem*, char type, char format);


//------------------------------Display----------------------------------------
// Display the contents of the entire library
	bool displayContents() const;

private:

	int hash(char) const; // index for both dimensions of collections

	BinTree * collections; //arrays of trees of InventoryItems(Format X Type)
	
};

#endif
