//-----------------------------------------------------------------------------
// DispHistory.cpp
// **DispHistory member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------


#include "DispHistory.h"

//------------------------------constructor------------------------------------
DispHistory::DispHistory() { 
	user = NULL;
	performed = false;
}

//---------------------------------set-----------------------------------------
//  Set data members of Action
//  Preconditions:  A valid Library array and patron array has been created, 
//					the input line contains the users ID
//					ex. "8888"
//
//  Postconditions: Patron with the associated ID is assigned to user, 
//                  if the user is not NULL and the action has not been 
//					performed, true is returned
bool DispHistory::set(string &s, Library* l, Patron** p){
	
	if( !performed && l != NULL && p != NULL ){
		//Assign patron user
		int end = s.find(' ');					// Find end of patron number
		string patronNumberString = s.substr(0,end); // Extract patron number
		int patronNumber = stoi2(patronNumberString); // Convert to int

		if( patronNumber >= 0 && patronNumber < kPatronSize ){
			user = p[patronNumber]; // Assign patron to user
			userID = patronNumber;  // Assign ID number to userID
			if (user != NULL) {
				return true;
			}
		}
	}

	return false;
}

//--------------------------------perform--------------------------------------
//  Perform the action, Displaying the history of actions for user
//  Preconditions:  set has been called and is successful
//  Postconditions: The action displays the user's history of actions
bool DispHistory::perform(){

	bool result = false;

	if( !performed && user != NULL ){
		//Output patron ID as a 4 digit number with leading 0's
		cout << "*** Patron ID = " << setw(kPatronNumberW) << setfill('0') 
			 << right << userID << setfill(' ') << "  ";
		
		//Mark as performed
		performed = true;

		//Display user history
		result = user->dispHistory();
	}
	//Remove from memory as the action is not needed for a record
	delete this;

	return result;
}

//------------------------------- display -------------------------------------
// Display the action type
// Preconditions:  The action has been set with valid data
// Postconditions: "Display Patron History" is output as the identifier for 
//				   this action
void DispHistory::display() const{

	cout << "Display Patron History" << endl;
}

//------------------------------- create --------------------------------------
//  Produce a new action of the same type as this action
//  Preconditions:  This has been properly constructed.
//  Postconditions: A pointer to a new action of this type is returned.
//					NOTE: User is responsible for deleting this new action.
Action* DispHistory::create() const{
	
	return new DispHistory();

}
