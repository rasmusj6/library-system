//-----------------------------------------------------------------------------
// DispLibrary.h
// **Derived DispLibrary class
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef _DISPLIBRARY_H_
#define _DISPLIBRARY_H_

#include "Action.h"

//---------------------------DispLibrary Class---------------------------------
// DispLibrary Class: Class to record a display of a Library's history
//	-- set function to Set data members of Action
//	-- perform function to execute operations of the action, 
//     displaying the contents of the library
//
// Assumptions:
//	-- Action objects are created by a system with at least one Library and 
//     at least one Patron
//-----------------------------------------------------------------------------
class DispLibrary: public Action{
public:

//------------------------------constructor------------------------------------
	DispLibrary();

//---------------------------------set-----------------------------------------
//  Set data members of Action
	bool set(string &s, Library* l, Patron** p);

//--------------------------------perform--------------------------------------
//  Perform the action
	bool perform();

//--------------------------------display--------------------------------------
//  Display the contents of the action
	void display() const;

	Action* create() const;

private:
	Library* lib;
};

#endif
