//-----------------------------------------------------------------------------
// bintree.cpp
// **BinTree class member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------
// BinTree Class: BinTree is a sorted collection of InventoryItem objects.
//	-- operations included for insert, remove, retrieve, and display
//
// Assumptions:
//	-- InventoryItem will handle its own comparison operators
//	-- Duplicate items are not inserted into tree
//-----------------------------------------------------------------------------


#include "bintree.h"


//-----------------------------------------------------------------------------
//  constructor:  sets NULL root
//-----------------------------------------------------------------------------
BinTree::BinTree() : root(NULL) { }  

//-----------------------------------------------------------------------------
//  destructor:     calls makeEmpty
//-----------------------------------------------------------------------------
BinTree::~BinTree(){

	makeEmpty();
} 

//-----------------------------------------------------------------------------
//  isEmpty:        True if tree is empty
//  Precondition:   None
//  Postconditions: Returns true if the tree is empty
//-----------------------------------------------------------------------------
bool BinTree::isEmpty() const{

	return root == NULL;
} 

//-----------------------------------------------------------------------------
//  makeEmpty:      Removes all nodes and data from the tree
//  Precondition:   None
//  Postconditions: Tree is empty, tree has no nodes and data is deleted
//-----------------------------------------------------------------------------
void BinTree::makeEmpty(){

	if ( !isEmpty() ) makeEmptyHelper(root);
} 

// makeEmpty Helper function
void BinTree::makeEmptyHelper(Node*& n){

    if (n->left != NULL)
        makeEmptyHelper(n->left);
    if (n->right != NULL)
        makeEmptyHelper(n->right);
	delete n->data;
    delete n;
    n = NULL;
}


//-----------------------------------------------------------------------------
//  Insert:         New node is inserted into the tree pointing to the data 
//                  provided, successful insert returns true
//  Precondition:   none
//  Postconditions: The new data is inserted into the BSTree in the correct 
//                  place in the tree. Successful insert returns true
//-----------------------------------------------------------------------------
bool BinTree::insert(InventoryItem* item){

	bool inserted = false;

	if( item == NULL ){
		return inserted;
	}
	else if (root == NULL){ 
		root = new Node(item); 
		inserted = true;
	}
	else { 
		Node *AddNode = new Node(item);
		Node *cur = root; 
		while ( !inserted && cur != NULL) {
			if (*item < *cur->data){ // Item belongs to left tree
				if (cur->left == NULL) { 
					//Left child is empty insert to left
					cur->left = AddNode; 
					inserted = true; 
				} 
				else {
					//Left child is occupied, move to left child
					cur = cur->left;
				} 
			}
			else if (*item > *cur->data){ // Item belongs to right sub tree
				if (cur->right == NULL) { 
					//Right child is empty insert to left
					cur->right = AddNode; 
					inserted = true; 
				} 
				else {
					//Right child is occupied, move to right child
					cur = cur->right; 
				}
			}
			else { // Return False if data is already in tree
				cur = NULL;
				delete AddNode;
				AddNode = NULL;
			}
		} 
	} 
	
	return inserted;
}

//-----------------------------------------------------------------------------
//  remove:         Removes the InventoryItem ToRemove from the tree and Removed
//                  points to the removed data; User must delete the removed 
//                  data if desired.
//  Precondition:   None
//  Postconditions: The Node data is removed from the tree and Removed points 
//                  to the data. Returns true if the data is found and removed.
//-----------------------------------------------------------------------------
bool BinTree::remove(const InventoryItem & ToRemove, InventoryItem*& Removed){
	return removeHelper(root, ToRemove, Removed);
}

// remove helper function
bool BinTree::removeHelper(Node*& Current, const InventoryItem& ToRemove, 
						   InventoryItem*& Removed){

	if ( Current == NULL ){ // Case for empty tree
		return false;
	}
	else if ( *Current->data == ToRemove ){
		//remove it
		Removed = Current->data;
		deleteRoot(Current);
		return true;
	}
	else if ( *Current->data > ToRemove ){
		return removeHelper(Current->left, ToRemove, Removed);
	}
	else{
		return removeHelper(Current->right, ToRemove, Removed);
	}

}

// removeHelper helper function: Used to remove the node from the tree
void BinTree::deleteRoot(Node *&root) { 

	if (root->left == NULL && root->right == NULL) {  
		delete root; 
		root = NULL; 
	} 
	else if (root->left == NULL) { 
		Node *tmp = root; 
		root = root->right;  
		delete tmp; 
	} 
	else if (root->right == NULL) { 
		Node *tmp = root; 
		root = root->left; 
		delete tmp; 
	} 
	else { 
		root->data = findAndDeleteMostLeft(root->left); 
	} 
 }

// deleteRoot helper function: finds the next smallest value and removes it
InventoryItem* BinTree::findAndDeleteMostLeft(Node *&root) { 

	if (root->right == NULL) { 
		InventoryItem *item = root->data; 
		Node *junk = root; 
		root = root->left; 
		delete junk; 
		return item; 
	} 
	else {
		return findAndDeleteMostLeft(root->right); 
	}
} 


//-----------------------------------------------------------------------------
//  retrieve:       Finds target in the search tree and assigns it to found. 
//                  True is returned if the target is found
//  Precondition:   None
//  Postconditions: found points to the searched target data, returns true if
//                  the target was retrieved.
//-----------------------------------------------------------------------------
bool BinTree::retrieve(const InventoryItem & target, InventoryItem*& found) const{

	Node* curr = root;
	found = NULL;

	while(curr != NULL && found == NULL){
		if( *curr->data == target ){
			found = curr->data;
		}
		else if( *curr->data < target ){
			curr = curr->right;
		}
		else{
			curr = curr->left;
		}
	}
	return (found != NULL);
}

void BinTree::display() const {

	displayHelper(root);
}

// displaySideways helper function
void BinTree::displayHelper(const Node* current) const {

   if (current != NULL) {
	   displayHelper(current->left);

	   current->data->lDisplay();
	   
	   displayHelper(current->right);

   }
}
