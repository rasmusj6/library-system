//-----------------------------------------------------------------------------
// ItemFactory.h
// **Description Needed
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef _ITEMFACTORY_H_
#define _ITEMFACTORY_H_

#include "YouthBook.h"
#include "FictionBook.h"
#include "Periodical.h"

using namespace std;

//-----------------------------------------------------------------------------
// ItemFactory Class: Class to provide InventoryItems objects based on codes
//	-- produce returns a new InventoryItem obj based on input code
//	-- InventoryItems supported:
//			Y: YouthBook
//			F: FictionBook
//			P: MonthlyPeriodical
//
// Assumptions:
//	-- Fixed amount of available InventoryItem types
//	-- User is responsible for deallocating InventoryItems objs that 
//     are produced
//-----------------------------------------------------------------------------
class ItemFactory{

public:

//------------------------------constructor------------------------------------
// Initialize array of InventoryItems
	ItemFactory();

//-------------------------------destructor------------------------------------
// Properly deallocate memory
	~ItemFactory();

//--------------------------------produce--------------------------------------
//  produce a new item based on the code and return a pointer to the item
	InventoryItem* produce(char) const;

	int hash(char in) const;

private:

//--------------------------Private Data Members-------------------------------

	InventoryItem* factory[kSize]; //Array of pointers to Items used as a hash factory
};

#endif
