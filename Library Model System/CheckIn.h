//-----------------------------------------------------------------------------
// CheckIn.h
// **Derived CheckIn Class
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef _CHECKIN_H_
#define _CHECKIN_H_

#include "Action.h"

//----------------------------CheckIn Class------------------------------------
// CheckIn Class: Class to record a check in between the lib and patron
//	-- set function to Set data members of Action
//	-- perform function to execute operations of the action, checking in the 
//     appropriate book from the appropriate user
//
// Assumptions:
//	-- Action objects are created by a system with at least one Library and 
//     at least one Patron
//-----------------------------------------------------------------------------
class CheckIn: public Action{
public:

//------------------------------constructor------------------------------------
	CheckIn();

//---------------------------------set-----------------------------------------
//  Set data members of Action
	bool set(string &s, Library* l, Patron** p);

//--------------------------------perform--------------------------------------
//  Perform the action
	bool perform();

//--------------------------------display--------------------------------------
//  Display the contents of the action
	void display() const;

	Action* create() const;
private:

//--------------------------Private Data Members-------------------------------
	InventoryItem* item;
	Patron* user;
};

#endif
