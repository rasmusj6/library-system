//-----------------------------------------------------------------------------
// LibSystem.h
// **LibSystem controller class
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef _LIBSYSTEM_H_
#define _LIBSYSTEM_H_


#include "ActionFactory.h"
#include "ItemFactory.h"
#include "Library.h"
#include "bintree.h"
#include "Patron.h"
#include <iostream>
#include <fstream>

using namespace std;

//-------------------------------LibSystem Class-------------------------------
// LibSystem Class: Controller class for the system of libraries.
//	-- Run Reads files for collections, patrons, and actions and executes 
//     corresponding operations.
//	-- All output is displayed to the console
//
// Assumptions:
//	-- Input files must be properly formatted
//	-- May contain multiple libraries, defaults to one main library of items
//	-- All library controls are file input/not user controlled
//  -- Patrons are limited to 4 diget unique ID codes
//  -- Collection Items are limited to YouthBooks, FictionBooks, 
//     and Periodicals
//-----------------------------------------------------------------------------
class LibSystem{

public:
//-------------------------- Constructor --------------------------------------
// Dynamically allocates libraries and patrons
	LibSystem(); 

//-------------------------- Destructor ---------------------------------------
// deallocates libraries and patrons
	~LibSystem();

//-------------------------- run ----------------------------------------------
// Main system operations. Reads input files in order and executes operations 
// as they are listed in the files. All functionality is controlled inside run.
	void run();

private:
//-------------------------- loadPatronFile -----------------------------------
	bool loadPatronFile(const char* fileName);

//-------------------------- loadItemFile -------------------------------------
	bool loadItemFile(const char* fileName);

//-------------------------- loadActionFile -----------------------------------
	bool loadActionFile(const char* fileName);

//-------------------------- createPatron -------------------------------------
	void createPatron(ifstream &);

//-------------------------- performAction ------------------------------------
	void performAction(ifstream &);

//-------------------------- registerItem -------------------------------------
	void registerItem(ifstream &);

//--------------------------------hashP----------------------------------------
// Hash index for Patrons
	int hashP(int) const;

//--------------------------Private Data Members-------------------------------
	Library* lib; // Pointer to Library

	Patron** patrons; // Array of patron pointers

	ActionFactory aFactory; // Factory to create new actions

	ItemFactory iFactory; // Factory to create new items
};

#endif
