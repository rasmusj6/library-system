//-----------------------------------------------------------------------------
// Driver.cpp
// **Main driver function for Lab3
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------
// main: Driver function for LibSystem functionality exposition
//	-- Operates LibSystem and outputs to Console
//
// Assumptions:
//	-- All input files are formatted correctly and follow all assumptions 
//     of LibSystem
//-----------------------------------------------------------------------------


//#define _CRTDBG_MAP_ALLOC
//#include <stdlib.h>
//#include <crtdbg.h>


#include "LibSystem.h"
#include <iostream>

using namespace std;

int main(){
	//_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);  
	//_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG | _CRTDBG_MODE_WNDW);
	
	LibSystem Lib1;

	Lib1.run();

	//cin.get();
	return 0;

}
