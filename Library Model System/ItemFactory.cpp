//-----------------------------------------------------------------------------
// ItemFactory.cpp
// **ItemFactory member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#include "ItemFactory.h"

//-------------------------- hash ---------------------------------------------
// Hashes a character to an int
// Precondition:  Passed parameter is a char 
// Postcondition: char is mapped to int and returned
int ItemFactory::hash(char in) const{

	int i = in - 'A';
	if (i >= 0 && i < kSize) {
		return i;
	}
	return -1;
}

//-------------------------- constructor --------------------------------------
// Constructs an ItemFactory
// Precondition:  None
// Postcondition: array of InventoryItem is initialized to kSize (number of 
//				  different types of InventoryItems) and hashing the types of 
//				  new InventoryItems to places inside the factory array
ItemFactory::ItemFactory(){
	
	//Initialize all to NULL
	for ( int i = 0; i < kSize; i++){
		factory[i] = NULL;
	}

	//Assign dummy copies for copies
	factory[hash(kYouthBook)]	= new YouthBook();
	factory[hash(kFictionBook)] = new FictionBook();
	factory[hash(kPeriodical)]	= new Periodical();
}

//-------------------------- destructor ---------------------------------------
// Deletes an ItemFactory
// Precondition:  Properly constructed ItemFactory
// Postcondition: factory array of InventoryItems is deleted
ItemFactory::~ItemFactory(){

	for( int i = 0; i < kSize; i ++ ) {
		if( factory[i] != NULL ){
			delete factory[i];
			factory[i] = NULL;
		}
	}

}

//-------------------------- produce ------------------------------------------
// Returns an InventoryItem pointer to InventoryItem of type "in" (parameter)
// Precondition:  char passed is a valid char
// Postcondition: returned pointer is to InventoryItem of type "in", return 
//				  NULL if type of InventoryItem not found
InventoryItem* ItemFactory::produce(char in) const{

	int index = hash(in);

	if( index > 0 && factory[index] != NULL ) {
		return factory[index]->create();
	}
	else {
		return NULL;
	}
}
