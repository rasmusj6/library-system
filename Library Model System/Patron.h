//-----------------------------------------------------------------------------
// Patron.h
// **Patron class definition
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef _PATRON_H_
#define _PATRON_H_

#include "Action.h"
#include "InventoryItem.h"
#include <list>
#include <iostream>

using namespace std;

class InventoryItem;
class Action;

//-----------------------------------------------------------------------------
// Patron Class: A library patron that contains a collection of held items and 
//               a history of actions performed by the patron
//	-- Provides functionality to addActions to history, checkIn/out Items, and 
//     display the history of library actions performed
//
// Assumptions:
//	-- Patron will be controlled by a controller that will manage the items 
//     and actions
//-----------------------------------------------------------------------------
class Patron{

public:

//----------------------------constructor--------------------------------------
// constructs the Patron based on an input string of their full name
	Patron(string&);

//-----------------------------destructor--------------------------------------
	~Patron();

//-----------------------------addAction---------------------------------------
// Adds the action to its history
	bool addAction(Action*); 

//-----------------------------checkOut----------------------------------------
// Adds Item to itemsOut
	bool checkOut(InventoryItem*); 

//-----------------------------checkIn-----------------------------------------
// Removes from itemsOut
	bool checkIn(InventoryItem*);

//----------------------------dispHistory--------------------------------------
// Displays history of actions performed by the patron, most recent to least
	bool dispHistory();

private:

//--------------------------Private Data Members-------------------------------

	string firstName;

	string lastName;

	list<Action*> actionsHist;

	list<InventoryItem*> itemsOut;

};

#endif
