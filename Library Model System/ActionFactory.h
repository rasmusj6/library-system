//-----------------------------------------------------------------------------
// ActionFactory.h
// **ActionFactor class definition
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef _ACTIONFACTORY_H_
#define _ACTIONFACTORY_H_

#include "Action.h"
#include "CheckIn.h" 
#include "CheckOut.h"
#include "DispHistory.h"
#include "DispLibrary.h"


using namespace std;

//--------------------------ActionFactory Class--------------------------------
// ActionFactory Class: Class to provide Action objects based on hash codes
//	-- produce returns a new action obj based on input code
//	-- Actions supported:
//			R: CheckIn/Return
//			C: CheckOut
//			H: Display Patron History
//			D: Display Library contents
//
// Assumptions:
//	-- Fixed amount of available actions
//	-- User is responsible for deallocating Action objs that are produced
//-----------------------------------------------------------------------------
class ActionFactory{

public:

//------------------------------constructor------------------------------------
// Initialize array of actions
	ActionFactory();

//-------------------------------destructor------------------------------------
// Properly deallocate memory
	~ActionFactory(); 

//--------------------------------produce--------------------------------------
//  produce a new Action based on the code and return a pointer to the Action
	Action* produce(char) const;


private:
//----------------------------------hash---------------------------------------
//  Hash the input char for the appropriate factory index
	int hash(char) const;

//--------------------------Private Data Members-------------------------------

//Array of pointers to Actions used as a hash factory
	Action* factory[kSize]; 

};

#endif
