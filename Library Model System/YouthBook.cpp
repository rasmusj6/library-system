//-----------------------------------------------------------------------------
// YouthBook.cpp
// **YouthBook member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#include "YouthBook.h"

// Empty constructor
YouthBook::YouthBook() {}

// Empty destructor
YouthBook::~YouthBook(){}

//-------------------------- < operator ---------------------------------------
// Compares two YouthBooks to see if one is less then the other, first by
// title, then by author
// Precondition:  Both YouthBooks are valid YouthBooks and both are properly
//				  constructed/set
// Postcondition: bool returned if passed YouthBook is "less" than "this"
//				  YouthBook
bool YouthBook::operator<(const InventoryItem &b) const {

	const YouthBook& YouthB = static_cast<const YouthBook&>(b);

	if (title == YouthB.title) {
		return author < YouthB.author;
	} else {
		return title < YouthB.title;
	}
}

//-------------------------- > operator ---------------------------------------
// Compares two YouthBooks to see if one is more then the other, first by
// title, then by author
// Precondition:  Both YouthBooks are valid YouthBooks and both are properly
//				  constructed/set
// Postcondition: bool returned if passed YouthBook is "more" than "this"
//				  YouthBook
bool YouthBook::operator>(const InventoryItem &b) const {

	const YouthBook& YouthB = static_cast<const YouthBook&>(b);

	if (title == YouthB.title) {
		return author > YouthB.author;
	} else {
		return title > YouthB.title;
	}
}

//-------------------------- == operator --------------------------------------
// Compares two YouthBooks to see if they are equal to each other
// Precondition:  Both YouthBooks are valid YouthBooks and both are properly
//				  constructed/set
// Postcondition: bool returned if passed YouthBook is equal to "this"
//				  YouthBook
bool YouthBook::operator==(const InventoryItem &b) const {

	const YouthBook& YouthB = static_cast<const YouthBook&>(b);

	return title == YouthB.title && author == YouthB.author;
}

//-------------------------- != operator --------------------------------------
// Compares two YouthBooks to see if they are not equal to each other
// Precondition:  Both YouthBooks are valid YouthBooks and both are properly
//				  constructed/set
// Postcondition: bool returned if passed YouthBook is not equal to "this"
//				  YouthBook
bool YouthBook::operator!=(const InventoryItem &b) const {
	return !(*this == b);
}

//-------------------------- <= operator --------------------------------------
// Compares two YouthBooks to see if one is less or equal then the other, first
// by title, then by author
// Precondition:  Both YouthBooks are valid YouthBooks and both are properly
//				  constructed/set
// Postcondition: bool returned if passed YouthBook is "less" or equal to 
//				  "this" YouthBook
bool YouthBook::operator<=(const InventoryItem &b) const {
	return (*this == b) || (*this < b);
}

//-------------------------- >= operator --------------------------------------
// Compares two YouthBooks to see if one is more or equal then the other, first
// by title, then by author
// Precondition:  Both YouthBooks are valid YouthBooks and both are properly
//				  constructed/set
// Postcondition: bool returned if passed YouthBook is "more" or equal to 
//				  "this" YouthBook
bool YouthBook::operator>=(const InventoryItem &b) const {
	return (*this == b) || (*this > b);
}

//-------------------------- create() -----------------------------------------
// Creates YouthBook and returns a pointer to it
// Precondition:  none
// Postcondition: return pointer to new YouthBook
InventoryItem* YouthBook::create() const{
	return new YouthBook();
}

//-------------------------- searchSet() --------------------------------------
// Sets "this" YouthBook's varibles to a provided search string
// Precondition:  "this" YouthBook was properly constructed and passed search
//				  string is properly formated
//				  ex. "Walker Alice, The Color Purple,"
// Postcondition: YouthBook's author title and year are properly set. Return 
//				  true if successful 
bool YouthBook::searchSet(string &input) {

	// splits string on commas
	int first = input.find(','); // finds end of author
	int second = input.find(',', first+1); // finds end of title

	if (first >= 0 && second >= 0) {
		title = input.substr(0, first);
		author = input.substr(first+2, second-first-2);
		year = 0;
		return true;
	}
	return false;
}
