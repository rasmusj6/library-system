//-----------------------------------------------------------------------------
// DispHistory.h
// **Derived DispHistory class
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef _DISPHISTORY_H_
#define _DISPHISTORY_H_

#include "Action.h"

//--------------------------DispHistory Class----------------------------------
// DispHistory Class: Class to record a display of a patron's history
//	-- set function to Set data members of Action
//	-- perform function to execute operations of the action, 
//     displaying user history
//
// Assumptions:
//	-- Action objects are created by a system with at least one Library and 
//     at least one Patron
//-----------------------------------------------------------------------------
class DispHistory: public Action{

public:

//------------------------------constructor------------------------------------
	DispHistory();

//---------------------------------set-----------------------------------------
//  Set data members of Action
	bool set(string &s, Library* l, Patron** p);

//--------------------------------perform--------------------------------------
//  Perform the action
	bool perform();

//--------------------------------display--------------------------------------
//  Display the contents of the action
	void display() const;

	Action* create() const;

private:

//--------------------------Private Data Members-------------------------------
	Patron* user;

	int userID;
};

#endif
