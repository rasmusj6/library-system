#ifndef CONSTANTS
#define CONSTANTS

#include <string>

using namespace std;

static const char kYouthBook   = 'Y';
static const char kFictionBook = 'F';
static const char kPeriodical  = 'P';

static const int kSize = 26;
static const int kPatronSize = 10000;
static const int kAuthorOutWidth = 22;
static const int kTitleOutWidth = 40;
static const int kDateOutWidth = 5;
static const int kMonthOutWidth = 2;
static const int kAvailIndent = 6;

static const char kFormat = 'H';

static const int kPeriodicalTotal = 1;
static const int kBookTotal = 5;

static const int kPatronNumberW = 4;

static const char kCheckIn     = 'R';
static const char kCheckOut    = 'C';
static const char kPatronHist  = 'H';
static const char kLibraryDisp = 'D';

int stoi2(const string&);

#endif
