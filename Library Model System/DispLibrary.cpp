//-----------------------------------------------------------------------------
// DispLibrary.cpp
// **DispLibrary member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------


#include "DispLibrary.h"

//------------------------------constructor------------------------------------
DispLibrary::DispLibrary() { 
	lib = NULL;
	performed = false;
}

//---------------------------------set-----------------------------------------
//  Set data members of Action
//  Preconditions:  A valid Library array has been created, 
//					the input line is ignored and may contain anything
//  Postconditions: Library l is assigned to the actions lib, if the library 
//                  is not NULL and the action has not been performed, 
//                  true is returned
bool DispLibrary::set(string &s, Library* l, Patron** p){
	
	 lib = l;
	 return (!performed && l != NULL);
}

//--------------------------------perform--------------------------------------
//  Perform the action, Displaying the contents of the library
//  Preconditions:  set has been called and is successful
//  Postconditions: The action displays the specific library and then removes 
//                  itself from memory
bool DispLibrary::perform(){

	bool result = false; // return value container

	if( !performed && lib != NULL ){

		cout << "Library Contents: " << endl;

		performed = true; // Mark as performed

		//Display lib contents and set result to true if successful
		result = (lib->displayContents());
	}

	delete this; // Delete from memory as it is no longer needed

	return result; // return if successful
}

//------------------------------- display -------------------------------------
// Display the action type if it has been performed
// Preconditions:  The action has been set with valid data and marked performed
// Postconditions: "DispLibrary" is output as the identifier for this action
void DispLibrary::display() const{

	cout << "DispLibrary" << endl;
}

//------------------------------- create --------------------------------------
//  Produce a new action of the same type as this action
//  Preconditions:  This has been properly constructed.
//  Postconditions: A pointer to a new action of this type is returned.
//					NOTE: User is responsible for deleting this new action.
Action* DispLibrary::create() const{
	
	return new DispLibrary();
}
