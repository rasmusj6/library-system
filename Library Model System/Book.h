//-----------------------------------------------------------------------------
// Book.h
// **Derived class Book
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef BOOK_H
#define BOOK_H

#include "InventoryItem.h"

using namespace std;


//-----------------------------------------------------------------------------
// Book Class: A derived class intermediary between InventoryItem and all
//             types of books
//	-- Adds data members for title, author, and date
//	-- Defines display for all books
//  -- Defines set for all books
//
// Assumptions:
//	-- Operators defined by derived classes, otherwise books act the same as 
//     other books
//-----------------------------------------------------------------------------
class Book: public InventoryItem{

public:

//----------------------------constructor--------------------------------------
	Book();

//-----------------------------destructor--------------------------------------
	virtual ~Book();

//------------------------------Display----------------------------------------
// Outputs information about any book, Title, Author, Date
	virtual void lDisplay();

	virtual void pDisplay();

	virtual bool set(string&);
//-----------------------------Operators---------------------------------------
	//virtual bool operator< (InventoryItem &b) const = 0;
	//virtual bool operator> (InventoryItem &b) const = 0;
	//virtual bool operator==(InventoryItem &b) const = 0;
	//virtual bool operator!=(InventoryItem &b) const = 0;
	//virtual bool operator<=(InventoryItem &b) const = 0;
	//virtual bool operator>=(InventoryItem &b) const = 0;

protected:

	string title;

	string author;

	int year;

};

#endif
