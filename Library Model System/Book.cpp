//-----------------------------------------------------------------------------
// Book.cpp
// **Book member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------


#include "Book.h"

//-------------------------- constructor --------------------------------------
// Constructs Book
// Precondition:  None
// Postcondition: totalCnt is set to kBookTotal = 5
Book::Book(){
	totalCnt = kBookTotal;
}

// empty destructor
Book::~Book(){}

//-------------------------- set() --------------------------------------------
// Sets "this" Book's varibles to a provided string
// Precondition:  "this" Book was properly constructed and passed string
//				  is properly formated
//				  ex. "Muller Marcia, Edwin of the Iron Shoes, 1977"
// Postcondition: Book's author, title, and year are properly set. Return true
//				  if successful 
bool Book::set(string &input) {

	// splits string on commas
	int first = input.find(','); // finds end of author
	int second = input.find(',', first+1); // finds end of title

	if (first >= 0 && second >= 0 && (input.size()-second-1) >= 0 ) {
		author = input.substr(0, first);
		title = input.substr(first+2, second-first-2);
		year = stoi2(input.substr(second+2));
		return true;
	}
	return false;
}

//-------------------------- lDisplay() ---------------------------------------
// Is the library's version of display, displays available number of books, 
// author, title, then year
// Precondition:  Book is a valid Book and is properly constructed/set
// Postcondition: Book properly displayed
void Book::lDisplay(){

	cout << " " << left << setw(kAvailIndent) << totalCnt - checkedOut 
		<< setw(kAuthorOutWidth) <<  author.substr(0, kAuthorOutWidth - 1) 
		<< setw(kTitleOutWidth) << title.substr(0, kTitleOutWidth - 1) 
		<< setw(kDateOutWidth) << year << endl;
}

//-------------------------- pDisplay() ---------------------------------------
// Is the patrons's version of display, displays author, title, then year
// Precondition:  Book is a valid Book and is properly constructed/set
// Postcondition: Book properly displayed
void Book::pDisplay(){

	cout << " " << left << setw(kAuthorOutWidth) 
		<<  author.substr(0, kAuthorOutWidth - 1) 
		<< setw(kTitleOutWidth) << title.substr(0, kTitleOutWidth - 1) 
		<< setw(kDateOutWidth) << year << endl;
}
