//-----------------------------------------------------------------------------
// Patron.cpp
// **Patron member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#include "Patron.h"

//-------------------------- constructor --------------------------------------
// Properly constructs a patron from a given string
// Precondition:  String pass is properly formated
//				  ex "Bear Bertha"
// Postcondition: lastName and firstName are properly set
Patron::Patron(string &input) {

	// string is split on space
	int first = input.find(' '); // finds end of last name

	lastName = input.substr(0, first);
	firstName = input.substr(first+1);
}

//-------------------------- destructor ---------------------------------------
// Deletes a patron
// Precondition:  Patron is properly constructed
// Postcondition: Patron is completely deleted
Patron::~Patron() {

	// Clears list of items checked out
	itemsOut.clear();

	// Patron contains record of actions and must delete them
	list<Action*>::iterator itr2 = actionsHist.begin();

	while(itr2 != actionsHist.end() ){
		delete (*itr2);
		itr2++;
	}
}

//-------------------------- checkOut() ---------------------------------------
// Adds a InventoryItem to the Patrons list of items checked out to them
// Precondition:  Patron and InventoryItem are both valid and properly
//				  constructed
// Postcondition: item is added to the end Patrons itemsOut list. Return true
//				  if successful 
bool Patron::checkOut(InventoryItem* item) {
	if( item != NULL ) {
		itemsOut.push_back(item);
	}
	return item != NULL;
}

//-------------------------- checkIn() ----------------------------------------
// Removes a InventoryItem from the Patrons list of items checked out to them
// Precondition:  Patron and InventoryItem are both valid and properly
//				  constructed
// Postcondition: item is removed to Patrons itemsOut list. Return true
//				  if successful 
bool Patron::checkIn(InventoryItem* item) {
	if (item != NULL && !itemsOut.empty()) {
		list<InventoryItem*>::iterator itr2 = itemsOut.begin();

		while((*itr2) != item ){
			
			itr2++;
			if( itr2 == itemsOut.end()) break; // must be 2nd
		}
		
		if (itr2 != itemsOut.end()) {
			*itr2 = NULL;		   // set found item pointer to null
			itemsOut.remove(NULL); // remove all NULL's from list
			return true;
		}
	}
	return false;
}

//-------------------------- addAction() --------------------------------------
// Adds a Action to the Patrons list of actions the patron has done
// Precondition:  Patron and Action are both valid and properly constructed
// Postcondition: Action is added to the end Patrons itemsOut list. Return true
//				  if successful 
bool Patron::addAction(Action* act){
	if( act != NULL ){
		actionsHist.push_back(act);
	}
	return act != NULL;
}

//-------------------------- dispHistory --------------------------------------
// Displays the patrons name and the action history of the patron
// Precondition:  Patron is properly constructed
// Postcondition: Displays patron name and action history. Return true
//				  if successful 
bool Patron::dispHistory() {
	cout << lastName << " " << firstName << endl;

	list<Action*>::iterator itr2 = actionsHist.begin();

	// walks through action list and calls each actions display function
	while(itr2 != actionsHist.end() ){
		(*itr2)->display();
		itr2++;
	}
	cout << endl;
	return true;
}
