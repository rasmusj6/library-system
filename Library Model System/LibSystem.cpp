//-----------------------------------------------------------------------------
// LibSystem.cpp
// **LibSystem member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#include "LibSystem.h"
#include "constants.h"
#include <string>

const char* kBookFile = "data3books.txt";
const char* kPatronFile = "data3patrons.txt";
const char* kActionFile = "data3commands.txt";

//-------------------------- stoi2() -----------------------------------------
// Converts a string of numbers into a int
// Precondition:  string passed is a valid string of ints
// Postcondition: Return int of same "value" as passed string
int stoi2(const string& s){
	
	int total = 0;
	int length = s.length();
	int start = 0;
	int j = 0;
	// removes possible beginning spaces
	while(s[j] < '0' || s[j] > '9' ){
		++start;
		++j;
	}
	// converts string to total int
	for( int i = 0; i < length ; ++i ){
		total = (total * 10) + (s[i + start] - '0');
	}
	return total;

}

//-----------------------------Constructor-------------------------------------
// Dynamically allocates libraries and patrons
// Precondition:  NONE
// Postcondition: Lib and patrons are all properly constructed, all patrons 
//                are set to NULL
LibSystem::LibSystem(){

	lib = new Library();

	patrons = new Patron* [kPatronSize];

	for( int i = 0; i < kPatronSize; ++i ) patrons[i] = NULL;

} 

//-----------------------------Destructor--------------------------------------
// deallocates libraries and patrons
// Precondition:  NONE
// Postcondition: All memory is properly deallocated
LibSystem::~LibSystem(){ 

	for( int i = 0; i < kPatronSize; ++i ){

		delete patrons[i];
		patrons[i] = NULL;

	}

	delete [] patrons;

	delete lib;
	
	patrons = NULL;

	lib = NULL;

}

//---------------------------------run-----------------------------------------
// Main system operations. Reads input files in order and executes operations 
// as they are listed in the files. All functionality is controlled inside run.
// Precondition:  
// Postcondition: 
void LibSystem::run(){  

	if ( loadItemFile(kBookFile) ){

		if ( loadPatronFile(kPatronFile) ){


			if ( !loadActionFile(kActionFile) ){

				cout << "Error loading actions" << endl;
			}
		} else {
			cout << "Error loading patrons" << endl;
		}
	} else {
		cout << "Error loading items" << endl;
	}
}


//------------------------------loadPatronFile---------------------------------
// Read the patron file and create records for each patron
// Precondition:  fileName exists and it in the same folder and is properly 
//				  formated
// Postcondition: Patron array properly built with input data
bool LibSystem::loadPatronFile(const char* fileName){  

	ifstream infile(fileName);

	if ( infile.fail() ) return false;

	while( !infile.eof() ) {
		createPatron(infile); // Create patron from file information
	}
	infile.close();

	return true;
}

//------------------------------loadItemFile-----------------------------------
// Read the item file and create items for lib
// Precondition:  fileName exists and it in the same folder and is properly 
//				  formated
// Postcondition: Library properly built with input data
bool LibSystem::loadItemFile(const char* fileName){  

	ifstream infile(fileName);

	if( infile.fail() ) return false;

	while( !infile.eof() ) {
		registerItem(infile); //Create Item and registers in lib
	}
	infile.close();

	return true;
}

//------------------------------loadActionFile---------------------------------
// Read the Action file and performs actions on lib and patrons
// Precondition:  fileName exists and it in the same folder and is properly 
//				  formated
// Postcondition: Properly constructed actions and perfectly executed
bool LibSystem::loadActionFile(const char* fileName){  

	ifstream infile(fileName);

	if( infile.fail() ) return false;

	while( !infile.eof() ) {
		performAction(infile); // Creates and performs actions!
	}
	infile.close();

	return true;
}

//------------------------------createPatron-----------------------------------
// Read the file and adds one patron to array
// Precondition:  infile is open and is properly formated
// Postcondition: Properly constructed patron is created and added to array
void LibSystem::createPatron(ifstream &infile){
	int acctNumber = -1;
	string data;

	infile >> acctNumber;	// Read char for type
	infile.get();			// Skip space
	getline(infile, data);	// Read in rest of line
	if( acctNumber < kPatronSize && 
		acctNumber > 0 && 
		patrons[acctNumber] == NULL){

		patrons[acctNumber] = new Patron(data);//register patron
	}
	
}

//-----------------------------performAction-----------------------------------
// Read the Action file and performs an action on lib and patrons
// Precondition:  infile is open and is properly formated
// Postcondition: Properly constructed action and perfectly executed
void LibSystem::performAction(ifstream &infile){  

	char type = '0';
	string data;
	Action* ptr = NULL;

	infile >> type; // Read char for type

	infile.get();	// Skip space

	getline(infile, data); // Read in rest of line

	//Produce empty item
	ptr = aFactory.produce(type);

	if( ptr != NULL ){

		//Set item and then perform, if either fails, delete ptr
		if( !(ptr->set(data, lib, patrons) && ptr->perform() )) {
			delete ptr;
		}
		ptr = NULL;
	}
}

//-----------------------------registerItem------------------------------------
// Read the file and adds one item to lib
// Precondition:  infile is open and is properly formated
// Postcondition: Properly constructed item is created and added to lib
void LibSystem::registerItem(ifstream &infile){  

	char type = '0';
	string data;
	InventoryItem* ptr = NULL;

	infile >> type; // Read char for type

	infile.get();	// Skip space

	getline(infile, data); // Read in rest of line

	//Produce empty item
	ptr = iFactory.produce(type);

	if( ptr != NULL ){

		//Set item
		ptr->set(data);
		//Register in collections
		if ( !lib->registerItem(ptr, type, kFormat)) delete ptr;

		ptr = NULL;
	}
}


//--------------------------------hashP----------------------------------------
// Hash index for Patrons
int LibSystem::hashP(int num) const{  
	return num;
} 
