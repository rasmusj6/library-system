//-----------------------------------------------------------------------------
// FictionBook.h
// **Derived class FictionBook
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef FICTIONBOOK_H
#define FICTIONBOOK_H

#include "Book.h"

using namespace std;

//-----------------------------------------------------------------------------
// FictionBook Class: Derived class of book
//	-- Defines comparison operators for FictionBooks(Author then Title)
//
// Assumptions:
//	-- receives display functionality and set functionality from Book
//-----------------------------------------------------------------------------
class FictionBook: public Book{

public:

//----------------------------constructor--------------------------------------
	FictionBook();

//-----------------------------destructor--------------------------------------
	virtual ~FictionBook();

	virtual bool searchSet(string&);

	virtual InventoryItem* create() const;

//------------------------------Operators--------------------------------------
	virtual bool operator<(const InventoryItem &b) const; 
	virtual bool operator>(const InventoryItem &b) const;
	virtual bool operator==(const InventoryItem &b) const;
	virtual bool operator!=(const InventoryItem &b) const;
	virtual bool operator<=(const InventoryItem &b) const; 
	virtual bool operator>=(const InventoryItem &b) const;

};

#endif
