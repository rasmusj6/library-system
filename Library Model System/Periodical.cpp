//-----------------------------------------------------------------------------
// Periodical.cpp
// **Periodical member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------
#include "Periodical.h"

//-------------------------- constructor --------------------------------------
// Constructs pericodical
// Precondition:  None
// Postcondition: totalCnt is set to kPeriodicalTotal = 1
Periodical::Periodical() {
	totalCnt = kPeriodicalTotal;
}

// empty destructor
Periodical::~Periodical(){}

//-------------------------- < operator ---------------------------------------
// Compares two Periodicals to see if one is less then the other, first by
// title, then by author
// Precondition:  Both Periodical are valid Periodicals and both are properly
//				  constructed/set
// Postcondition: bool returned if passed Periodical is "less" than "this"
//				  Periodical
bool Periodical::operator<(const InventoryItem &b) const {
	
	const Periodical& perB = static_cast<const Periodical&>(b);
	if (year == perB.year) {
		if(month == perB.month) {
			return title < perB.title;
		}
		return month < perB.month;
	} else {
		return year < perB.year;
	}
}

//-------------------------- > operator ---------------------------------------
// Compares two Periodicals to see if one is more then the other, first by
// title, then by author
// Precondition:  Both Periodicals are valid Periodicals and both are properly
//				  constructed/set
// Postcondition: bool returned if passed Periodical is "more" than "this"
//				  Periodical
bool Periodical::operator>(const InventoryItem &b) const {
	const Periodical& perB = static_cast<const Periodical&>(b);
	if (year == perB.year) {
		if(month == perB.month) {
			return title > perB.title;
		}
		return month > perB.month;
	} else {
		return year > perB.year;
	}
}

//-------------------------- == operator --------------------------------------
// Compares two Periodicals to see if they are equal to each other
// Precondition:  Both Periodicals are valid Periodicals and both are properly
//				  constructed/set
// Postcondition: bool returned if passed Periodical is equal to "this"
//				  Periodical
bool Periodical::operator==(const InventoryItem &b) const {
	const Periodical& perB = static_cast<const Periodical&>(b);
	return title == perB.title && year == perB.year && month == perB.month;
}

//-------------------------- != operator --------------------------------------
// Compares two Periodicals to see if they are not equal to each other
// Precondition:  Both Periodicals are valid Periodicals and both are properly
//				  constructed/set
// Postcondition: bool returned if passed Periodical is not equal to "this"
//				  Periodical
bool Periodical::operator!=(const InventoryItem &b) const {
	return !(*this == b);
}

//-------------------------- <= operator --------------------------------------
// Compares two Periodicals to see if one is less or equal then the other,
// first by title, then by author
// Precondition:  Both Periodicals are valid Periodicals and both are properly
//				  constructed/set
// Postcondition: bool returned if passed Periodical is "less" or equal to 
//				  "this" Periodical
bool Periodical::operator<=(const InventoryItem &b) const {
	return (*this == b) || (*this < b);
}

//-------------------------- >= operator --------------------------------------
// Compares two Periodicals to see if one is more or equal then the other, 
// first by title, then by author
// Precondition:  Both Periodicals are valid Periodicals and both are properly
//				  constructed/set
// Postcondition: bool returned if passed Periodical is "more" or equal to 
//				  "this" Periodical
bool Periodical::operator>=(const InventoryItem &b) const {
	return (*this == b) || (*this > b);
}

//-------------------------- set() --------------------------------------------
// Sets "this" Periodical's varibles to a provided string
// Precondition:  "this" Periodical was properly constructed and passed string
//				  is properly formated
//				  ex. "Communications of the ACM, 2 2010"
// Postcondition: Periodical's title, month, and year are properly set. Return
//				  true if successful 
bool Periodical::set(string &input) {

	// splits string on commas
	int first = input.find(','); // finds end of title
	int second = input.find(' ', first+2); // finds end of date

	if (first >= 0 && second >= 0 && (input.size()-second-1) >= 0 ) {
		title = input.substr(0, first);
		string sMonth = input.substr(first+2, second-first-2);
		string sYear = input.substr(second+1);
		month = stoi2(sMonth);
		year = stoi2(sYear);
		return true;
	}
	return false;
}

//-------------------------- searchSet() --------------------------------------
// Sets "this" Periodical's varibles to a provided search string
// Precondition:  "this" Periodical was properly constructed and passed search
//				  string is properly formated
//				  ex. "2009 7 Sigart Bulletin,"
// Postcondition: Periodical's title, month, and year are properly set. Return
//				  true if successful 
bool Periodical::searchSet(string &input) {

	// splits string on spaces
	int first = input.find(' '); // finds end of month
	int second = input.find(' ', first+2); // finds end of year

	if (first >= 0 && second >= 0) {
		string sMonth = input.substr(first+1, second-first-1);
		string sYear = input.substr(0, first);
		month = stoi2(sMonth);
		year = stoi2(sYear);
		title = input.substr(second+1, input.size()-2-second);
		return true;
	}
	return false;
}

//-------------------------- create() -----------------------------------------
// Creates Periodical and returns a pointer to it
// Precondition:  none
// Postcondition: return pointer to new Periodical
InventoryItem* Periodical::create() const{
	return new Periodical();
}

//-------------------------- lDisplay() ---------------------------------------
// Is the library's version of display, displays available number of books, 
// year, month, then title
// Precondition:  Periodical is a valid Periodical and is properly
//				  constructed/set
// Postcondition: Periodical properly displayed
void Periodical::lDisplay() {

	cout  << " " << setw(kAvailIndent) << totalCnt - checkedOut 
		<< setw(kDateOutWidth) << year << setw(kMonthOutWidth) 
		<< right << month << left<< " " << title << endl;
}

//-------------------------- pDisplay() ---------------------------------------
// Is the patrons's version of display, displays year, month, then title
// Precondition:  Periodical is a valid Periodical and is properly
//				  constructed/set
// Postcondition: Periodical properly displayed
void Periodical::pDisplay() {

	cout << left << setw(1) << " " << setw(kDateOutWidth) << year 
		<< setw(kMonthOutWidth) << right << month << left << " " 
		<< title << endl;
}
