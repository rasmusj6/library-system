//-----------------------------------------------------------------------------
// FictionBook.cpp
// **FictionBook member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------


#include "FictionBook.h"

// empty constructor
FictionBook::FictionBook() {}

// empty destructor
FictionBook::~FictionBook(){}

//-------------------------- < operator ---------------------------------------
// Compares two FictionBooks to see if one is less then the other, first by
// title, then by author
// Precondition:  Both FictionBooks are valid FictionBooks and both are
//				  properly constructed/set
// Postcondition: bool returned if passed FictionBook is "less" than "this"
//				  FictionBook
bool FictionBook::operator<(const InventoryItem &b) const {
	
	const FictionBook& fictionB = static_cast<const FictionBook&>(b);

	if (author == fictionB.author) {
		return title < fictionB.title;
	} else {
		return author < fictionB.author;
	}
}

//-------------------------- > operator ---------------------------------------
// Compares two FictionBooks to see if one is more then the other, first by
// title, then by author
// Precondition:  Both FictionBooks are valid FictionBooks and both are 
//				  properly constructed/set
// Postcondition: bool returned if passed FictionBook is "more" than "this"
//				  FictionBook
bool FictionBook::operator>(const InventoryItem &b) const {

	const FictionBook& fictionB = static_cast<const FictionBook&>(b);

	if (author == fictionB.author) {
		return title > fictionB.title;
	} else {
		return author > fictionB.author;
	}
}

//-------------------------- == operator --------------------------------------
// Compares two FictionBooks to see if they are equal to each other
// Precondition:  Both FictionBooks are valid FictionBooks and both are 
//				  properly constructed/set
// Postcondition: bool returned if passed FictionBook is equal to "this"
//				  FictionBook
bool FictionBook::operator==(const InventoryItem &b) const {

	const FictionBook& fictionB = static_cast<const FictionBook&>(b);

	return title == fictionB.title && author == fictionB.author;
}

//-------------------------- != operator --------------------------------------
// Compares two FictionBooks to see if they are not equal to each other
// Precondition:  Both FictionBooks are valid FictionBooks and both are 
//				  properly constructed/set
// Postcondition: bool returned if passed FictionBook is not equal to "this"
//				  FictionBook
bool FictionBook::operator!=(const InventoryItem &b) const {
	return !(*this == b);
}

//-------------------------- <= operator --------------------------------------
// Compares two FictionBooks to see if one is less or equal then the other, 
// first by title, then by author
// Precondition:  Both FictionBooks are valid FictionBooks and both are 
//				  properly constructed/set
// Postcondition: bool returned if passed FictionBook is "less" or equal to 
//				  "this" FictionBook
bool FictionBook::operator<=(const InventoryItem &b) const {
	return (*this == b) || (*this < b);
}

//-------------------------- >= operator --------------------------------------
// Compares two FictionBooks to see if one is more or equal then the other,
// first by title, then by author
// Precondition:  Both FictionBooks are valid FictionBooks and both are
//				  properly constructed/set
// Postcondition: bool returned if passed FictionBook is "more" or equal to 
//				  "this" FictionBook
bool FictionBook::operator>=(const InventoryItem &b) const {
	return (*this == b) || (*this > b);
}

//-------------------------- searchSet() --------------------------------------
// Sets "this" FictionBook's varibles to a provided search string
// Precondition:  "this" FictionBook was properly constructed and passed search
//				  string is properly formated
//				  ex. "Kerouac Jack, On the Road,"
// Postcondition: FictionBook's author title and year are properly set. Return
//				  true if successful 
bool FictionBook::searchSet(string &input) {

	// splits string on commas
	int first = input.find(','); // finds end of author
	int second = input.find(',', first+1); // finds end of title

	if (first >= 0 && second >= 0) {
		author = input.substr(0, first);
		title = input.substr(first+2, second-first-2);
		year = 0;
		return true;
	}
	return false;
}

//-------------------------- create() -----------------------------------------
// Creates FictionBook and returns a pointer to it
// Precondition:  none
// Postcondition: return pointer to new FictionBook
InventoryItem* FictionBook::create() const{
	return new FictionBook();
}
