//-----------------------------------------------------------------------------
// InventoryItem.h
// **Base class for all Inventory Items
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef _INVENTORY_ITEM_H_
#define _INVENTORY_ITEM_H_

//#include "date.h"
#include "constants.h"
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

//-----------------------------------------------------------------------------
// InventoryItem Class: Base class for all Inventory Items
//	-- set initializes all data members based on string input(from a file)
//	-- display outputs the contents of the item to the console
//	-- operators compare items as defined by derived classes
//	-- checkin and checkout mark copies as they come in/leave library
//
// Assumptions:
//	-- all virtual functions are defined by derived classes
//-----------------------------------------------------------------------------
class InventoryItem{
	
public:

//------------------------------constructor------------------------------------
	InventoryItem();

//-------------------------------destructor------------------------------------
	virtual ~InventoryItem();

//--------------------------------set------------------------------------------
// Initialize data members based on string input
	virtual bool set(string&) = 0;

//-------------------------------display---------------------------------------
// Display contents of Item
	virtual void lDisplay() = 0;

	virtual void pDisplay() = 0;

	virtual InventoryItem* create() const = 0;

//------------------------------operators--------------------------------------
	virtual bool operator< (const InventoryItem &b) const = 0;
	virtual bool operator> (const InventoryItem &b) const = 0;
	virtual bool operator==(const InventoryItem &b) const = 0;
	virtual bool operator!=(const InventoryItem &b) const = 0;
	virtual bool operator<=(const InventoryItem &b) const = 0;
	virtual bool operator>=(const InventoryItem &b) const = 0;

//------------------------------checkIn----------------------------------------
	bool checkIn();

//------------------------------checkOut---------------------------------------
	bool checkOut();

protected:

	int totalCnt; // Hard copy total owned by library

	int checkedOut; // Hard copied checked out

	char format; // e.g. 'H' for hard copy

};

#endif
