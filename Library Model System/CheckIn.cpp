//-----------------------------------------------------------------------------
// CheckIn.cpp
// **CheckIn member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------


#include "CheckIn.h"

//------------------------------constructor------------------------------------
CheckIn::CheckIn() { 
	user = NULL;
	item = NULL;
	performed = false;
}

//---------------------------------set-----------------------------------------
//  Set data members of Action
//  Preconditions:  A valid Library array and patron array has been created, 
//					the input line formatted as:
//                  <patronNumber>' '<ItemTypeCode>' '<formatCode>' '
//                  <primarySearchCriteria>", "<secondarySearchCriteria>','
//					ex. "8888 F H Lawrence D. H., Lady Chatterley's Lover,"
//
//  Postconditions: Library l is assigned to the actions lib, the patron 
//					defined by patronNumber is assigned to user, 
//					and the library item is assigned to item
bool CheckIn::set(string &s, Library* l, Patron** p){
	
	if( !performed && l != NULL && p != NULL ){
		//Assign patron user
		int end = s.find(' ');					// Find end of patron number
		string patronNumberString = s.substr(0,end); // Extract patron number
		int patronNumber = stoi2(patronNumberString); // Convert to int

		if( patronNumber >= 0 && patronNumber < kPatronSize ){
			user = p[patronNumber]; // Assign patron

			if (user != NULL) {
				string s2 = s.substr(5); // Extract Item search criteria
				//Assign item
				item = l->find(s2);		// Find the item
				if( item != NULL ) return true;
			}
		}
	}

	return false;
}

//--------------------------------perform--------------------------------------
//  Perform the action checking out the item to the user
//  Preconditions:  CheckIn has been properly set
//  Postconditions: Item is checked out to user, and this action is included 
//                  in the users history
bool CheckIn::perform(){

	if( !performed && user != NULL && item != NULL ){
		performed = true;

	//mark checked out && add to patrons collection && add to patrons history
		return ( user->checkIn(item) &&		// attempt to remove from user
				 item->checkIn() &&			// attempt to return library
				 user->addAction(this));	// attempt to add action to history
	}else
		return false;
}

//--------------------------------display--------------------------------------
//  Display the contents of the action
//  Preconditions:  The action has been set with valid data and performed
//  Postconditions: The action identifies itself as a CheckIn and displays 
//					the items contents
void CheckIn::display() const{

	if(performed){
		cout << "CheckIn  ";
		item->pDisplay();
	}
}

//------------------------------- create --------------------------------------
//  Produce a new action of the same type as this action
//  Preconditions:  This has been properly constructed.
//  Postconditions: A pointer to a new action of this type is returned.
//					NOTE: User is responsible for deleting this new action.
Action* CheckIn::create() const{
	
	return new CheckIn(); // return pointer to new copy of same Action
}
