//-----------------------------------------------------------------------------
// ActionFactory.cpp
// **ActionFactory member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------


#include "ActionFactory.h"

//-------------------------- hash ---------------------------------------------
// Hashes a character to an int
// Precondition:  Passed parameter is a char 
// Postcondition: char is mapped to int and returned
int ActionFactory::hash(char in) const{

	int i = in - 'A';
	if (i >= 0 && i < kSize) {
		return i;
	}
	return -1;
}

//-------------------------- constructor --------------------------------------
// Constructs an ActionFactory
// Precondition:  None
// Postcondition: array of Actions is initialized to kSize (number of 
//				  different types of Action) and hashing the types of 
//				  new Action to places inside the factory array
ActionFactory::ActionFactory(){
	
	//Initialize all to NULL
	for ( int i = 0; i < kSize; i++){
		factory[i] = NULL;
	}

	//Assign dummy copies for copies
	factory[hash(kCheckIn)]	= new CheckIn();
	factory[hash(kCheckOut)] = new CheckOut();
	factory[hash(kPatronHist)]	= new DispHistory();
	factory[hash(kLibraryDisp)]	= new DispLibrary();
}

//-------------------------- destructor ---------------------------------------
// Deletes an ActionFactory
// Precondition:  Properly constructed ActionFactory
// Postcondition: factory array of Actions is deleted
ActionFactory::~ActionFactory(){

	for( int i = 0; i < kSize; i ++ ) {
		if( factory[i] != NULL ){
			delete factory[i];
			factory[i] = NULL;
		}
	}
}

//-------------------------- produce ------------------------------------------
// Returns an Action pointer to Action of type "in" (parameter)
// Precondition:  char passed is a valid char
// Postcondition: returned pointer is to Action of type "in", return 
//				  NULL if type of Action not found
Action* ActionFactory::produce(char in) const{

	int index = hash(in);

	if( index > 0 && factory[index] != NULL ) {
		return factory[index]->create();
	}
	else {
		return NULL;
	}
}
