//-----------------------------------------------------------------------------
// Periodical.h
// **Derived class Periodical
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#ifndef PERIODICAL_H
#define PERIODICAL_H

#include "InventoryItem.h"

using namespace std;



//-----------------------------------------------------------------------------
// Periodical Class: A derived class intermediary between InventoryItem and all
//             types of Periodicals
//	-- Adds data members for title and date
//	-- Defines display for all Periodicals
//  -- Defines set for all Periodicals
//  -- Defines comparison operators for Periodical
//     (Date(year, then month) then title)
//
// Assumptions:
//	-- Operators defined by derived classes, otherwise books act the same as 
//     other books
//-----------------------------------------------------------------------------
class Periodical: public InventoryItem{

public:

//----------------------------constructor--------------------------------------
	Periodical();

//-----------------------------destructor--------------------------------------
	virtual ~Periodical();

//-------------------------------set-------------------------------------------
	bool set(string&);

	bool searchSet(string&);
//------------------------------Display----------------------------------------
// Outputs information for any periodical, title and date
	virtual void lDisplay();

	virtual void pDisplay();

	InventoryItem* create() const;
//-----------------------------Operators---------------------------------------
	virtual bool operator< (const InventoryItem &b) const;
	virtual bool operator> (const InventoryItem &b) const;
	virtual bool operator==(const InventoryItem &b) const;
	virtual bool operator!=(const InventoryItem &b) const;
	virtual bool operator<=(const InventoryItem &b) const;
	virtual bool operator>=(const InventoryItem &b) const;

protected:

	string title;

	int month;

	int year;

};

#endif
