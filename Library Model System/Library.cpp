//-----------------------------------------------------------------------------
// Library.cpp
// **Library member function implementation
// Authors: Corwin Marsh, James Rasmussen, 
// CSS 502 Winter 2014 : Lab 3
//-----------------------------------------------------------------------------

#include "Library.h"

//-------------------------- constructor --------------------------------------
// Constructs a Library
// Precondition:  None
// Postcondition: array of bintree is initialized to kSize (number of 
//				  different types of InventoryItems)
Library::Library() {
	collections = new BinTree[kSize];
}

//-------------------------- destructor ---------------------------------------
// Deletes a library
// Precondition:  Properly constructed Library
// Postcondition: collections array of bintrees is deleted
Library::~Library() {

	delete [] collections;
	collections = NULL;

}

//-------------------------- hash ---------------------------------------------
// Hashes a character to an int
// Precondition:  Passed parameter is a char 
// Postcondition: char is mapped to int and returned
int Library::hash(char h) const {
	int i = h - 'A';
	if (i >= 0 && i < kSize) { // if "i" isnt in array size
		return i;
	}
	return -1;
}

//-------------------------- find ---------------------------------------------
// Finds InventoryItem in collections
// Precondition:  Passed string is in valid format
//				  ex. "F H Kerouac Jack, On the Road,"
//				  ex. "Y H Danny Dunn & the Homework Machine, Williams Jay,"
//				  ex. "P H 2010 2 Communications of the ACM,"
// Postcondition: String is split up and made into corresponding InventoryItem,
//				  found in collections and a pointer is returned to that item
InventoryItem* Library::find(string& input) const {

	char type = input[0];

	InventoryItem* found = NULL;

	string s2;

	if (input[2] == kFormat) {
	//really good if-else solution
		if (type == kFictionBook) {
			FictionBook f;
			s2 = input.substr(4);
			f.searchSet(s2);
			// find fictionbook string in fictionbook bintree
			collections[hash(kFictionBook)].retrieve(f, found);
			return found;

		}else if (type == kYouthBook) {
			YouthBook f;
			s2 = input.substr(4);
			f.searchSet(s2);
			// find youthbook string in youthbook bintree
			collections[hash(kYouthBook)].retrieve(f, found);
			return found;

		}else if (type == kPeriodical) {
			Periodical f;
			s2 = input.substr(4);
			f.searchSet(s2);
			// find periodical string in periodical bintree
			collections[hash(kPeriodical)].retrieve(f, found);
			return found;
		}else{
			return found;
		}
	} else {
		return NULL;
	}
}

//-------------------------- registerItem -------------------------------------
// Adds InventoryItem to collections array
// Precondition:  Properly constructed InventoryItem, type and format are 
//				  valid
// Postcondition: input is added to correct bintree in collections. Return
//				  true if successful
bool Library::registerItem(InventoryItem* input, char type, char format) {
	int index = hash(type);
	if( index >= 0 )
		return collections[index].insert(input);
	else
		return false;
}

//-------------------------- desplayContents ----------------------------------
// Displays the contents of bintrees in collections array, including amount of
// each there is available to "check out"
// Precondition:  Properly constructed Library
// Postcondition: Contents of bintrees in collections array displayed. Return
//				  true if successful
bool Library::displayContents() const {
	
	if (collections != NULL) { 
		int index = 0;

		// Displays FictionBook bintree
		cout << "Fiction" << endl;
		cout << left << setw(kAvailIndent) << "AVAIL" << " " 
			<< setw(kAuthorOutWidth) << "AUTHOR" << setw(kTitleOutWidth) 
			<< "TITLE" << setw(kDateOutWidth) <<"YEAR" << endl;
		index = hash(kFictionBook);
		collections[index].display();
		cout << endl;

		// Display Periodicals bintree
		cout << "Periodicals" << endl;
		cout << left << setw(kAvailIndent) << "AVAIL" << " " << setw(kDateOutWidth) << "YEAR" << setw(kMonthOutWidth) << "MO" << " " << setw(kTitleOutWidth) << "TITLE" << endl;
		index = hash(kPeriodical);
		collections[index].display();
		cout << endl;

		// Display YouthBook bintree
		cout << "YouthBook" << endl;
		cout << left << setw(kAvailIndent) << "AVAIL" << " " << setw(kAuthorOutWidth) << "AUTHOR" << setw(kTitleOutWidth) << "TITLE" << setw(kDateOutWidth) <<"YEAR" << endl;
		index = hash(kYouthBook);
		collections[index].display();
		cout << endl;
		return true;
	}

	return false;
}
