### What is this repository for? ###

This model library system automates the process of tracking books. To start the library only has 3 types of books (children's books, fiction and periodicals). It allows “patrons” to check out books, return books, to display the contents of the library and display a report of a patron's history at any time. All books are displayed by category, sorted within the category. Output formatted for easy reading with data in columns. This program initializes the contents of each category of book and process an arbitrary sequence of checkouts, returns, and displays of different kinds of information.

For more information on Program Design or Implementation see

LibraryClassDiagram.pdf and LibrarySystemDocumentation.pdf in master

### How do I get set up? ###

Current version has only run in Visual Studio 2010 and has not be made to be and executable file. To run in VS2010 

*   Load VS2010

*   Start a new project ("example")
*   Right Click on 
        "Solution 'example'" 
     in solution explorer window and click on 
       "Open folder in Windows Explorer"
*   Add Library System files in the folder brought up in windows explorer
*   After including the files in VS2010 run Driver.cpp to produce output. 
*   If errors occur with loading the initialization files (data3books.txt, data3commands.txt, data3patrons.txt) then files were not properly included into project.

To test program, change initialization files (data3books.txt, data3commands.txt, data3patrons.txt) to see how program reacts to different input.

### Contribution guidelines ###

Remember this is very early stage Library System, Library doesn't contain very many books or patrons, but is designed for expandability. More book/library resources could be added to library and more actions could be added. 

### Who do I talk to? ###

For more information on the project contact me personally, james.l.rasmussen@gmail.com